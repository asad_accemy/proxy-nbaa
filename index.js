var http = require('http');
var request = require("request");
var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var requestify = require('requestify'); 
app.use(bodyParser.json());
var HttpClient = require('node-rest-client').Client;
var httpClient = new HttpClient();
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/proxy', function (req, res) {
	console.log(req.body);
   if (req.body.message === undefined) {
    // This is an error case, as "message" is required.
    res.status(400).send('No message defined!');
  } else {
    // Everything is okay.
    //console.log(req.body.message);
    var url = req.body.message;



// GET Call
var options = { method: 'GET',
  url: 'http://stats.nba.com/stats/leaguedashplayerstats',
  qs: 
   { College: '',
     Conference: '',
     Country: '',
     DateFrom: '',
     DateTo: '',
     Division: '',
     DraftPick: '',
     DraftYear: '',
     GameScope: '',
     GameSegment: '',
     Height: '',
     LastNGames: '0',
     LeagueID: '00',
     Location: '',
     MeasureType: 'Base',
     Month: '0',
     OpponentTeamID: '0',
     Outcome: '',
     PORound: '0',
     PaceAdjust: 'N',
     PerMode: 'PerGame',
     Period: '0',
     PlayerExperience: '',
     PlayerPosition: '',
     PlusMinus: 'N',
     Rank: 'N',
     Season: '2017-18',
     SeasonSegment: '',
     SeasonType: 'Regular Season',
     ShotClockRange: '',
     StarterBench: '',
     TeamID: '0',
     VsConference: '',
     VsDivision: '',
     Weight: '' } };
console.log("----");
request(options, function (error, response, body) {
  if (error) throw new Error(error);
  res.send(body);
});

//res.send("Hi");
   // res.status(200).send('Success: ' + req.body.message);
  }
})



var server = app.listen(8080, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)
});






